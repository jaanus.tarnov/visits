package ee.bcs.valiit.visits;

import java.text.SimpleDateFormat;
import java.util.Date;
//see mis siin kirjas on juhend, millega me loome objekte.

public class Visit extends Object implements Comparable{<Visit> {

    private Date date;
    private int count;

    // see on konstruktor
    public Visit () {}

    // see on kaa konstruktor
    public Visit (Date date, int count) {
        this.date = date;
        this.count = count;
    }

    public int getCount() {
       return count;
    }

    public void getCount( int count) {
        this.count = count;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyy");
        return date.toString() + ", " + count;
    }
    @Override
    public int compareTo(Visit o) {
        return 0;
    }
}
